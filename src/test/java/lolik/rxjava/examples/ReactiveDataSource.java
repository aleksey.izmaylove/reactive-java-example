package lolik.rxjava.examples;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import hu.akarnokd.rxjava2.parallel.ParallelTransformers;
import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

public class ReactiveDataSource {

    private static Flowable<Long> publisher() {
        return Flowable
                .generate(() -> new AtomicLong(0),
                        (BiFunction<AtomicLong, Emitter<Long>, AtomicLong>) (aLong, emitter) -> {
                            long value = aLong.getAndIncrement();
                            System.out.println(Thread.currentThread().getName() + " generate " + value);
                            emitter.onNext(value);
                            return aLong;
                        })
                .take(9999);
    }

    @Test
    public void parallelOrderedMerge() {
        ParallelTransformers.orderedMerge(
                publisher()
                        .parallel(Runtime.getRuntime().availableProcessors(), 1)
                        .runOn(Schedulers.computation())
                        .filter(ReactiveDataSource::test)
                        .map(ReactiveDataSource::apply))
                .skip(200)
                .take(50)
                .takeUntil(Flowable.timer(999, TimeUnit.MILLISECONDS))
                .doOnNext(aDouble -> System.out.println(Thread.currentThread().getName() + " result " + aDouble))
                .toList()
                .doOnSuccess(doubles -> System.out.println(Thread.currentThread().getName() + doubles))
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertValue(Arrays.asList(100.0, 100.5, 101.0, 101.5, 102.0, 102.5, 103.0, 103.5, 104.0, 104.5, 105.0, 105.5, 106.0, 106.5, 107.0, 107.5, 108.0, 108.5, 109.0, 109.5, 110.0, 110.5, 111.0, 111.5, 112.0, 112.5, 113.0, 113.5, 114.0, 114.5, 115.0, 115.5, 116.0, 116.5, 117.0, 117.5, 118.0, 118.5, 119.0, 119.5, 120.0, 120.5, 121.0, 121.5, 122.0, 122.5, 123.0, 123.5, 124.0, 124.5));
    }

    @Test
    public void concatEager() {
        publisher()
                .concatMapEager(aLong -> Flowable.just(aLong)
                        .subscribeOn(Schedulers.computation())
                        .filter(ReactiveDataSource::test)
                        .map(ReactiveDataSource::apply)
                )
                .skip(200)
                .take(50)
                .takeUntil(Flowable.timer(999, TimeUnit.MILLISECONDS))
                .doOnNext(aDouble -> System.out.println(Thread.currentThread().getName() + " result " + aDouble))
                .toList()
                .doOnSuccess(doubles -> System.out.println(Thread.currentThread().getName() + doubles))
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertValue(Arrays.asList(100.0, 100.5, 101.0, 101.5, 102.0, 102.5, 103.0, 103.5, 104.0, 104.5, 105.0, 105.5, 106.0, 106.5, 107.0, 107.5, 108.0, 108.5, 109.0, 109.5, 110.0, 110.5, 111.0, 111.5, 112.0, 112.5, 113.0, 113.5, 114.0, 114.5, 115.0, 115.5, 116.0, 116.5, 117.0, 117.5, 118.0, 118.5, 119.0, 119.5, 120.0, 120.5, 121.0, 121.5, 122.0, 122.5, 123.0, 123.5, 124.0, 124.5));
    }

    @Test
    public void streamPage() {
        publisher()
                .filter(ReactiveDataSource::test)
                .map(ReactiveDataSource::apply)
                .skip(200)
                .take(50)
                .takeUntil(Flowable.timer(9990, TimeUnit.MILLISECONDS))
                .doOnNext(aDouble -> System.out.println(Thread.currentThread().getName() + " result " + aDouble))
                .toList()
                .doOnSuccess(doubles -> System.out.println(Thread.currentThread().getName() + doubles))
                .test()
                .assertValue(Arrays.asList(100.0, 100.5, 101.0, 101.5, 102.0, 102.5, 103.0, 103.5, 104.0, 104.5, 105.0, 105.5, 106.0, 106.5, 107.0, 107.5, 108.0, 108.5, 109.0, 109.5, 110.0, 110.5, 111.0, 111.5, 112.0, 112.5, 113.0, 113.5, 114.0, 114.5, 115.0, 115.5, 116.0, 116.5, 117.0, 117.5, 118.0, 118.5, 119.0, 119.5, 120.0, 120.5, 121.0, 121.5, 122.0, 122.5, 123.0, 123.5, 124.0, 124.5));
    }

    @Test
    public void flatMapSort() {
        final int part = 1 + Runtime.getRuntime().availableProcessors();
        List<Double> list = publisher()
                .subscribeOn(Schedulers.io())
                .groupBy(aLong -> aLong % part)
                .flatMap(group -> group
                        .observeOn(Schedulers.computation())
                        .filter(ReactiveDataSource::test)
                        .map(ReactiveDataSource::apply))
                .takeUntil(Flowable.timer(999, TimeUnit.MILLISECONDS))
                .doOnNext(aDouble -> System.out.println(Thread.currentThread().getName() + " result " + aDouble))
                .toSortedList()
                .doOnSuccess(doubles -> System.out.println(Thread.currentThread().getName() + doubles))
                .blockingGet();
        List<Double> subList = list.subList(200, 250);
        assertEquals(Arrays.asList(100.0, 100.5, 101.0, 101.5, 102.0, 102.5, 103.0, 103.5, 104.0, 104.5, 105.0, 105.5, 106.0, 106.5, 107.0, 107.5, 108.0, 108.5, 109.0, 109.5, 110.0, 110.5, 111.0, 111.5, 112.0, 112.5, 113.0, 113.5, 114.0, 114.5, 115.0, 115.5, 116.0, 116.5, 117.0, 117.5, 118.0, 118.5, 119.0, 119.5, 120.0, 120.5, 121.0, 121.5, 122.0, 122.5, 123.0, 123.5, 124.0, 124.5), subList);
    }

    private static Double apply(Long l) {
        System.out.println(Thread.currentThread().getName() + " map " + l);
        return ((double) l) / 10;
    }

    private static boolean test(Long l) throws InterruptedException {
        Thread.sleep(1);
        boolean b = 0 == l % 5;
        if (b) System.out.println(Thread.currentThread().getName() + " filter " + l);
        return b;
    }
}