package lolik.rxjava.examples;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.functions.BiFunction;

public class ReactiveExceptions {

    private final long start = System.nanoTime();

    @Test
    public void exception() {
        final AtomicLong atomicLong = new AtomicLong(0);
        Flowable
                .generate(() -> new AtomicInteger(0),
                        (BiFunction<AtomicInteger, Emitter<Integer>, AtomicInteger>) (atomicInteger, emitter) -> {
                            int value = atomicInteger.getAndIncrement();
                            System.out.println(Thread.currentThread().getName() + " generate " + value);
                            if (355 == value) throw new RuntimeException();
                            emitter.onNext(value);
                            return atomicInteger;
                        })
                .onErrorResumeNext(Flowable.empty())
                .flatMap(integer -> Flowable.just(integer)
                        .filter(i -> {
                            if (0 == i % 20) {
                                throw new RuntimeException(atomicLong.incrementAndGet() + " filter failed");
                            }
                            return 0 == i % 5;
                        }).map(i -> {
                            if (0 == i % 330) throw new RuntimeException(atomicLong.incrementAndGet() + " map failed");
                            return ((double) i) / 10;
                        }).onErrorResumeNext(throwable -> {
                            System.out.println(throwable.getMessage());
                            return Flowable.empty();
                        }))
                .skip(45)
                .take(10)
                .takeUntil(Flowable.timer(999, TimeUnit.MILLISECONDS))
                .toList()
                .test()
                .assertValue(Arrays.asList(30.5, 31.0, 31.5, 32.5, 33.5, 34.5, 35.0));
    }

    @Test
    public void timeout() {
        final List<Double> doubles = publisher()
                .filter(this::test)
                .map(this::apply)
                .skip(200)
                .take(50)
                .takeUntil(Flowable.timer(999, TimeUnit.MILLISECONDS))
                .toList()
                .blockingGet();
        System.out.println((System.nanoTime() - start) / 1000000 + " " + Thread.currentThread().getName() + " did");
        assertThat(doubles, is(Arrays.asList(100.0, 100.5, 101.0, 101.5, 102.0, 102.5, 103.0, 103.5, 104.0, 104.5, 105.0, 105.5, 106.0, 106.5, 107.0, 107.5, 108.0, 108.5, 109.0, 109.5, 110.0, 110.5, 111.0, 111.5, 112.0, 112.5, 113.0, 113.5, 114.0, 114.5, 115.0, 115.5, 116.0, 116.5, 117.0, 117.5, 118.0, 118.5, 119.0, 119.5, 120.0, 120.5, 121.0, 121.5, 122.0, 122.5, 123.0, 123.5, 124.0, 124.5)));
    }

    private Double apply(Integer i) throws InterruptedException {
        if (1245 == i) {
            System.out.println((System.nanoTime() - start) / 1000000 + " " + Thread.currentThread().getName() + " sleeps");
            Thread.sleep(900);
        }
        return ((double) i) / 10;
    }

    private boolean test(Integer i) {
        return 0 == i % 5;
    }

    private Flowable<Integer> publisher() {
        return Flowable
                .generate(() -> new AtomicInteger(0),
                        (BiFunction<AtomicInteger, Emitter<Integer>, AtomicInteger>) (atomicInteger, emitter) -> {
                            int value = atomicInteger.getAndIncrement();
                            if (0 == value % 7) Thread.sleep(9);
                            System.out.println(Thread.currentThread().getName() + " generate " + value);
                            emitter.onNext(value);
                            return atomicInteger;
                        })
                .take(9999)
                ;
    }
}
